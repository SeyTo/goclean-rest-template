package main

import (
	"template-gorest/app/controller/user"
	"template-gorest/app/repo"
	users "template-gorest/app/service/user"
	app "template-gorest/lib"
	"template-gorest/lib/db"
	"template-gorest/lib/exception"
	"template-gorest/lib/middlewares"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func SetupControllers(repos *repo.Repos) []app.Controller {
	// Setup controllers
	ccs := make([]app.Controller, 0)
	ccs = append(ccs, &user.CustomerController{
		CustomerService: users.NewCustomerService(&repos.CustomerRepository),
	})
	return ccs
}

func SetupRepository(db *db.DB) {
}

func main() {

	// Setup repository and db

	// Setup server and routing
	ap := &app.App{
		AppName:             "goc",
		Prefix:              "goc",
		Router:              nil,
		ErrorHandler:        exception.ErrorHandler,
		ResponseInterceptor: middlewares.ResponseMiddleware,
		Middlewares: []func(c *fiber.Ctx) error{
			requestid.New(),
		},
	}

	exception.PanicIfNeeded(ap.New())

	repos := repo.NewRepo(&ap.DBs)
	ap.Controllers = SetupControllers(repos)

	ap.ListenAndServe()
}
