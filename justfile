dev-watch:
  GO_ENV=dev watchexec -r -i ./data -N --clear -d 600 -e go go run main.go

dev-db:
  GO_ENV=dev docker-compose --env-file .env.dev -f docker-compose.dev.yaml up goclean-dev-pgsql -d

dev-db-create-mig:
  migrate create -ext sql -dir db/.migrations -seq init

dev-db-mig-up:
  migrate -database ${POSTGRESQL_URL} -path db/.migrations up