package user

import (
	"template-gorest/app/repo"
)

type CustomerService struct {
	CustomerRepo *repo.CustomerRepository[repo.CustomerEntity]
}

func NewCustomerService(repo *repo.CustomerRepository[repo.CustomerEntity]) *CustomerService {
	return &CustomerService{
		CustomerRepo: repo,
	}
}

func (service *CustomerService) Profile(request any) (any, error) {
	// validation.Validate(request)
	// service.CustomerRepo.RepoImpl.Find()
	service.CustomerRepo.Find(nil)
	return "hello", nil
}

func (service *CustomerService) Create(request any) (any, error) {
	service.CustomerRepo.Create(repo.CustomerEntity{
		Username: "test",
	})
	return "created", nil
}
