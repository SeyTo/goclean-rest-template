package user

import (
	users "template-gorest/app/service/user"
	"template-gorest/lib"
	"template-gorest/lib/exception"
	"template-gorest/lib/model"

	"github.com/gofiber/fiber/v2"
)

type CustomerController struct {
	CustomerService *users.CustomerService
}

func NewCustomerController(customerService *users.CustomerService) *CustomerController {
	return &CustomerController{
		CustomerService: customerService,
	}
}

func (cc *CustomerController) Route(app *fiber.App) []lib.RouteDef {
	return []lib.RouteDef{
		{
			Name:       "Customer profile",
			Route:      "/u/profile",
			Method:     fiber.MethodGet,
			Controller: cc.Profile,
		},
		{
			Name:       "Create customer",
			Route:      "/u/customer",
			Method:     fiber.MethodPost,
			Controller: cc.Create,
		},
	}
}

func (cc *CustomerController) Profile(c *fiber.Ctx) error {
	// request.Id = uuid.New().String()

	// exception.PanicIfNeeded(err)

	cc.CustomerService.Profile(nil)

	return c.JSON(model.ResponseMeta{
		Code: "200",
		Data: "test",
	})
}

func (cc *CustomerController) Create(c *fiber.Ctx) error {
	var request CreateCustomerDto
	if err := c.BodyParser(&request); err != nil {
		// bad body
		return exception.ValidationError{
			Message: err.Error(),
			Err:     err,
		}
	}

	customer, err := cc.CustomerService.Create(request)
	if err != nil {
		return err
	}

	c.Locals("result", model.ResponseMeta{
		Code:    "200",
		Message: "User has been created",
		Data:    customer,
	})
	return c.Next()
}
