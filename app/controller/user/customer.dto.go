package user

type CreateCustomerDto struct {
	Username string `json:"username" validate:"required"`
}
