package repo

import (
	"log"
	"template-gorest/lib/db"
	"template-gorest/lib/repository"
)

type CustomerRepository[T CustomerEntity] struct {
	Db *db.DB
	*repository.RepositoryImpl[T]
}

type CustomerEntity struct {
	Username string
}

func (t *CustomerRepository[CustomerEntity]) Create(client CustomerEntity) (*CustomerEntity, error) {
	// Do something
	// t.db.create(client.username)
	log.Println("created from within CustomerRepo")
	return nil, nil
}

// func test() {
// 	c := &ClientRepository[ClientEntity]{}
// 	client := &ClientEntity{
// 		username: "test",
// 	}
// 	c.Create(*client)
// }
