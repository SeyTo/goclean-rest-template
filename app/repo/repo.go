package repo

import (
	"template-gorest/lib/db"
	"template-gorest/lib/repository"
)

type Repos struct {
	CustomerRepository CustomerRepository[CustomerEntity]
}

func NewRepo(db *db.DB) *Repos {
	return &Repos{
		CustomerRepository: CustomerRepository[CustomerEntity]{
			Db:       db,
			RepositoryImpl: &repository.RepositoryImpl[CustomerEntity]{},
		},
	}
}
