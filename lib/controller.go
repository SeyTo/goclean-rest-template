package lib

import "github.com/gofiber/fiber/v2"

type Controller interface {
	Route(app *fiber.App) []RouteDef
}

type Controllers []Controller
