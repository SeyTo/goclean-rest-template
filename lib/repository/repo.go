package repository

import "log"

type IEntity interface{}

type Repository[T IEntity] interface {
	Find(wheres any) (*T, error)
	Create(t T) (*T, error)
	CreateMany(t []T) ([]T, error)
}

type RepositoryImpl[T IEntity] struct{}

func (r *RepositoryImpl[T]) Find(wheres any) (*T, error) {
	log.Println("find from within Repoimpl")
	return nil, nil
}

func (t *RepositoryImpl[E]) Create(e E) (*E, error) {
	// Do something
	// t.db.create(client.username)
	log.Println("created from within Repoimpl")
	return nil, nil
}
