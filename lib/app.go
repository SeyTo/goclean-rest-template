package lib

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"template-gorest/lib/config"
	"template-gorest/lib/db"
	"time"

	"github.com/gofiber/fiber/v2"
)

type App struct {
	AppName             string
	Prefix              string
	Configurations      *config.Config
	ErrorHandler        func(ctx *fiber.Ctx, err error) error
	Router              any
	DBs                 db.DB
	Controllers         Controllers
	Middlewares         []func(c *fiber.Ctx) error
	ResponseInterceptor func(ctx *fiber.Ctx) error
}

func (app *App) New() error {
	// setup config
	// config.ParseFlags()
	// Setup configuration
	app.Configurations = config.New()

	// setup database
	app.DBs = *db.SetupDatabase(app.Configurations)

	// setup logging
	// setup metrics
	// setup database
	// setup cache
	// setup server
	// setup session
	// setup mailer
	// return app
	// setup routes

	// setup middlewares by config
	return nil
}

func (app *App) ListenAndServe() error {
	srv := fiber.New(fiber.Config{
		ErrorHandler: app.ErrorHandler,
		AppName:      app.AppName,
		IdleTimeout:  30 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 600 * time.Second,
	})

	// custom hooks
	srv.Hooks().OnRoute(func(r fiber.Route) error {
		log.Printf("Route: %s, (%s) ", r.Path, r.Method)
		return nil
	})

	// apply all pre-middlewares
	for _, m := range app.Middlewares {
		srv.Use(m)
	}

	// apply all routes
	for _, v := range app.Controllers {
		for _, r := range v.Route(srv) {
			srv.Add(r.Method, r.Route, append(r.PreMiddlewares, r.Controller)...)
		}
	}

	// final middleware
	if app.ResponseInterceptor != nil {
		srv.Use(app.ResponseInterceptor)
	}

	// TODO defer close all pools

	log.Printf("✅ Listening on port %s", os.Getenv("PORT"))
	go GracefulShutdown(srv, 10*time.Second)
	return srv.Listen(fmt.Sprintf("%s:%d", app.Configurations.Server.Host, app.Configurations.Server.Port))
}

func GracefulShutdown(app *fiber.App, timeout time.Duration) {
	stop := make(chan os.Signal, 1)

	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop

	log.Printf("🏁 Shutting down server with %s timeout", timeout)

	if err := app.Shutdown(); err != nil {
		log.Printf("❌ Error while shutting down server: %v", err)
	} else {
		log.Println("🏁 Server was shut down gracefully")
	}
}
