package lib

import (
	"github.com/gofiber/fiber/v2"
)

type RouteDef struct {
	Name           string
	Route          string
	Method         string
	Controller     func(c *fiber.Ctx) error
	PreMiddlewares [](func(c *fiber.Ctx) error)
}
