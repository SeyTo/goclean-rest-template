package exception

type ValidationError struct {
	Message string
	Err     error
}

func (validationError ValidationError) Error() string {
	return validationError.Message
}
