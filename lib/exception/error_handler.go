package exception

import (
	"log"
	"template-gorest/lib/model"

	"github.com/gofiber/fiber/v2"
)

func ErrorHandler(ctx *fiber.Ctx, err error) error {
	log.Println("at errorhandler")
	e, ok := err.(ValidationError)
	if ok {
		return ctx.JSON(model.ResponseMeta{
			Code:    "400",
			Message: "Bad validation",
			Log:     "Bad validation for input",
			Data:    e,
		})
	}

	return ctx.JSON(model.ResponseMeta{
		Code:    "500",
		Message: "Something went wrong",
		Log:     "Something went wrong",
		Data:    err.Error(),
	})
}
