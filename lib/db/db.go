package db

import (
	"fmt"
	"log"
	"net/url"
	"template-gorest/lib/config"
	"template-gorest/lib/db/postgres"

	"github.com/jmoiron/sqlx"
)

const (
	DB_TYPE_PG = 0
)

type DB struct {
	dbs map[string]*sqlx.DB
}

func (db *DB) Get(name string) *sqlx.DB {
	// TODO: on db disconnect will need to reconnect
	return db.dbs[name]
}

func SetupDatabase(configuration *config.Config) *DB {
	log.Println("🔌 Connecting database")

	if configuration == nil {
		panic("config is nil")
	}

	var dbs = make(map[string]*sqlx.DB)

	for name, options := range configuration.Databases {
		if !options.Enabled {
			continue
		}

		url := url.URL{
			Scheme: options.Scheme,
			User:   url.UserPassword(options.User, options.Pass),
			Host:   fmt.Sprintf("%s:%d", options.Host, options.Port),
		}

		// TODO parallel connect all dbs
		// will it try to restablish the network again
		db, err := ConnectDatabase(name, url, DB_TYPE_PG, options.Options)
		if err != nil {
			log.Println(err)
			// panic(err)
		}
		dbs[name] = db
	}

	// databases := configuration
	return &DB{
		dbs: dbs,
	}
}

func ConnectDatabase(name string, connectionString url.URL, dbType int, options map[string]interface{}) (*sqlx.DB, error) {
	query := url.Values{}
	for key, value := range options {
		query.Add(key, value.(string))
	}

	connectionString.RawQuery = query.Encode()

	switch dbType {
	case DB_TYPE_PG:
		db, err := postgres.New(name, connectionString)
		if err != nil {
			return nil, err
		}
		return db, nil
	default:
		panic(fmt.Sprintf("unknown database type with (%d)", dbType))
	}
}
