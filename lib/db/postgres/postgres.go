package postgres

import (
	"log"
	"net/url"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func New(name string, url url.URL) (*sqlx.DB, error) {
	// TODO reconnect try
	client, err := sqlx.Connect("postgres", url.String())

	if err != nil {
		log.Printf("Database connection error for (%s)", name)
		return nil, err
	}

	return client, nil
}
