package config

import (
	"flag"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"template-gorest/lib/exception"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

const ENV_PREFIX = "goc"

type Config struct {
	Server    ServerConfig
	Databases map[string]Database `json:"databases"`
}

type ServerConfig struct {
	Host string
	Port int
}

type Database struct {
	Scheme  string                 `json:"scheme"`
	Enabled bool                   `json:"enabled"`
	Host    string                 `json:"host"`
	Port    int                    `json:"port"`
	User    string                 `json:"user"`
	Pass    string                 `json:"pass"`
	Options map[string]interface{} `json:"options"`
}

/*
Create and load new config.
*/
func New() *Config {
	log.Println("🚩 Loading configs")
	flag.Parse()

	currentEnv := os.Getenv("GO_ENV")
	if currentEnv != "dev" && currentEnv != "stag" && currentEnv != "prod" {
		panic("No env configured. It should be either [dev, stag, prod])")
	}
	var file = fmt.Sprintf(".env.%s", currentEnv)

	err := godotenv.Load(file)
	exception.PanicIfNeeded(err)

	// load config from viper
	viper.BindEnv("go_env")
	viper.SetEnvPrefix(ENV_PREFIX)

	wd := os.Getenv("go_base")
	var configFilePath string = wd + "./config"
	viper.AddConfigPath(configFilePath)

	viper.SetConfigName("dev.config")
	viper.SetConfigType("json")
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	exception.PanicIfNeeded(err)
	configsStr := viper.AllSettings()
	adaptConfigsToEnvVars(&configsStr, "")

	var config Config
	err = viper.Unmarshal(&config)
	exception.PanicIfNeeded(err)

	return &config
}

/*
Replaces the viper's config value where value with ENV_PREFIX are
found.
*/
func adaptConfigsToEnvVars(configs *map[string]interface{}, parentKey string) {
	for key, value := range *configs {
		kind := reflect.ValueOf(value).Kind()

		if kind == reflect.Map {
			// if value is a map, recursively look into it
			val, ok := (value).(map[string]interface{})
			if ok {
				adaptConfigsToEnvVars(&val, fmt.Sprintf("%s.%s", parentKey, key))
			}
		} else if kind == reflect.String && strings.HasPrefix(value.(string), strings.ToUpper(ENV_PREFIX)) {
			// a string value maybe an environment variable, replace with the environment var here.
			foundEnvVal := os.Getenv(value.(string))
			if foundEnvVal != "" {
				// form the keys for viper and then assign the new value
				viper.Set(fmt.Sprintf("%s.%s", strings.TrimPrefix(parentKey, "."), key), foundEnvVal)
			}
		}
	}
}
