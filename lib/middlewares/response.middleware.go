package middlewares

import "github.com/gofiber/fiber/v2"

func ResponseMiddleware(c *fiber.Ctx) error {
	if v := c.Locals("result"); v != nil {
		return c.JSON(v)
	}
	return c.JSON("{}")
}
