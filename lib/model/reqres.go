package model

type Result[T interface{}] struct {
	Ok    T
	Error interface{}
}

type ResponseMeta struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Log     string      `json:"log"`
	TraceId string      `json:"trace_id"`
}
